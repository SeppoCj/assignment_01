﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockInteraction : MonoBehaviour
{
    [SerializeField] private GameObject screenBlock;
    [SerializeField] private float halfWidth, halfHeight;
    private GameObject blocksTemp;
    private Vector2 mP;
    private CapsuleCollider[] cC;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = -20; i <= 20; i++)
        {
            for (int j = -10; j <= 10; j++)
            {
                blocksTemp = Instantiate(screenBlock, new Vector3(.5f * i, .5f * j, 0), Quaternion.identity);
                blocksTemp.transform.rotation = Quaternion.Euler(90, 0, 0);

            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        //  for (int i = 0; i <= blocks.Length; i++) {
        //      blocks[i].SetActive(true);
        if (Input.GetMouseButtonDown(1)){
            Debug.Log("I pressed the button at :  " + Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10)));

            foreach (CapsuleCollider collisions in Physics.OverlapBox(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10)), new Vector3(halfWidth, halfHeight), Quaternion.identity, LayerMask.GetMask("Blocker")))
            {
               
                Debug.Log("Hello?!");
                collisions.gameObject.SetActive(false);
            }
        }
    }
}
