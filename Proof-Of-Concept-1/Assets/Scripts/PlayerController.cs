﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float m_horizontal;
    [SerializeField] private bool m_grounded = false, m_cameraSwitch = false;
    [SerializeField] private float m_speed;
    [SerializeField] private float m_jump;
    [SerializeField] private float m_layer = 0;
    [SerializeField] private Camera m_MainCamera;
    private Rigidbody m_rb;

    private void Start()
    {
        m_rb = GetComponent<Rigidbody>();
        m_rb.constraints = RigidbodyConstraints.FreezeRotation;
    }

    private void Update()
    {
        m_horizontal = Input.GetAxisRaw("Horizontal");
        if (m_cameraSwitch)
        {
            m_MainCamera.transform.position = new Vector3(0 + this.transform.position.x, 2.5f + this.transform.position.y, 10 + this.transform.position.z);
            m_MainCamera.transform.eulerAngles = new Vector3(10, 180, 0);
        }
        else
        {
            m_MainCamera.transform.position = new Vector3(0 + this.transform.position.x, 2.5f + this.transform.position.y, -10 + this.transform.position.z);
            m_MainCamera.transform.eulerAngles = new Vector3(10, 0, 0);
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            m_cameraSwitch = !m_cameraSwitch;

        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (!m_cameraSwitch)
            {
                m_layer++;
                if (m_layer >= 5)
                {
                    m_layer = 0;
                }
            }
            else
            {
                m_layer--;
                if (m_layer <= -1)
                {
                    m_layer = 5;
                }
            }
        }
        else if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            if (!m_cameraSwitch)
            {
                m_layer--;
                if (m_layer <= -1)
                {
                    m_layer = 5;
                }
            }
            else
            {
                m_layer++;
                if (m_layer >= 5)
                {
                    m_layer = 0;
                }

            }
        }

        switch (m_layer)
        {
            case 0:
                transform.position = new Vector3(transform.position.x, transform.position.y, 0);
                break;
            case 1:
                transform.position = new Vector3(transform.position.x, transform.position.y, 6);
                break;
            case 2:
                transform.position = new Vector3(transform.position.x, transform.position.y, 12);
                break;
            case 3:
                transform.position = new Vector3(transform.position.x, transform.position.y, 18);
                break;
            case 4:
                transform.position = new Vector3(transform.position.x, transform.position.y, 24);
                break;
        }

        if (m_grounded && Input.GetAxis("Jump") != 0)
        {
            m_rb.velocity = new Vector3(m_rb.velocity.x, 0f, 0f);
            m_rb.AddForce(new Vector3(0, m_jump, 0), ForceMode.Impulse);
            m_grounded = false;
        }

        m_grounded = Physics.Raycast(m_rb.transform.position, Vector3.down, GetComponent<CapsuleCollider>().height / 2 + Time.deltaTime, LayerMask.GetMask("Ground"));

    }

    private void FixedUpdate()
    {
        if (!m_cameraSwitch)
        {
            m_rb.velocity = new Vector3(m_horizontal * m_speed, m_rb.velocity.y, 0f);
        }
        else
        {
            m_rb.velocity = new Vector3(-m_horizontal * m_speed, m_rb.velocity.y, 0f);
        }
    }

}
